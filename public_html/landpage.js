var bigSquare = document.querySelector(".big-square");
var body = document.body;
var laptop = document.querySelector("#laptop-block");
var mheader = document.querySelector("h1");
var mdescr = document.querySelector("p.description");
var features = document.querySelectorAll(".feature");

window.onscroll = fixPage;

function fixPage() {
	if (window.innerWidth < 570) {
		bigSquare.style.boxShadow = "0 0 0px #00000085";
		laptop.style.paddingRight = "0px";
		mheader.style.transform = "translate(0px, 0)";
		mdescr.style.transform = "translate(0px, 0)";
		return;
	}

	if (window.scrollY < body.offsetWidth) {
		var ratio = window.scrollY / body.offsetWidth * 37.78860740502004;

		bigSquare.style.boxShadow = "0 0 " + ratio + "px #00000085";
		laptop.style.paddingRight = (ratio * 2) + "px";
		mheader.style.transform = "translate(" + 2* ratio + "px, 0)";
		mdescr.style.transform = "translate(" + ratio + "px, 0)";
	}

	for (var i = 0; i < features.length; i++) {
		var feature = features[i];
		var geometry = feature.querySelector(".photo").getBoundingClientRect();
		var step = geometry.width * 0.25;
		var bottom = window.innerHeight + step;

		if (geometry.top >= -step && geometry.bottom <= bottom) {
			feature.classList.add("active");
		}
		else {
			feature.classList.remove("active");
		}

		console.log(geometry.top, geometry.bottom);
	}

	console.log("");
}

fixPage()